package kr.co.jcsound.opencv_study.template_matching;

public class PGMException extends Exception{
    public PGMException(String message) {
        super(message);
    }
}
