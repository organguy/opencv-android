package kr.co.jcsound.opencv_study.image_compare;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import org.opencv.android.Utils;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FastFeatureDetector;
import org.opencv.imgproc.Imgproc;
import org.opencv.xfeatures2d.BriefDescriptorExtractor;

import kr.co.jcsound.ocvcannyedgedetection.R;
import kr.co.jcsound.opencv_study.OrganActivity;

public class FeatureMatchingActivity extends OrganActivity {

    public static int MATCHING_DISTANCE = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public int compareFeature(Mat img1, Mat img2){
        int retval = 0;
        long startTime = System.currentTimeMillis();

        MatOfKeyPoint keyPoint1 = new MatOfKeyPoint();
        MatOfKeyPoint keyPoint2 = new MatOfKeyPoint();

        Mat descriptors1 = new Mat();
        Mat descriptors2 = new Mat();

        FastFeatureDetector detector = FastFeatureDetector.create(FastFeatureDetector.FAST_N);
        BriefDescriptorExtractor extractor = BriefDescriptorExtractor.create();

        detector.detect(img1, keyPoint1);
        detector.detect(img2, keyPoint2);

        extractor.compute(img1, keyPoint1, descriptors1);
        extractor.compute(img2, keyPoint2, descriptors2);

        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);

        MatOfDMatch matches = new MatOfDMatch();

        if(descriptors2.cols() == descriptors1.cols()){
            matcher.match(descriptors1, descriptors2, matches);

            DMatch[] match = matches.toArray();
            double max_dist = 0;
            double min_dist = 100;

            for(int i = 0; i < descriptors1.rows(); i++){
                double dist = match[i].distance;
                if(dist < min_dist){
                    min_dist = dist;
                }

                if(dist > max_dist){
                    max_dist = dist;
                }
            }

            for(int i = 0; i < descriptors1.rows(); i++){
                if(match[i].distance <= MATCHING_DISTANCE){
                    retval++;
                }
            }

            Log.d("TAG", "max_dist : " + max_dist + " , " + "min_dist : " + min_dist);
        }

        long estimatedTime = System.currentTimeMillis() - startTime;

        Log.d("TAG", "matching Count : " + retval);
        Log.d("TAG", "estimatedTime : " + estimatedTime + "ms");


        return retval;
    }

    @Override
    public void initOpenCV() {
        Drawable drawable1 = getResources().getDrawable(R.drawable.kcsj_001, null);
        Drawable drawable2 = getResources().getDrawable(R.drawable.kcsj_091, null);

        Bitmap bitmap1 = ((BitmapDrawable)drawable1).getBitmap();
        Bitmap bitmap2 = ((BitmapDrawable)drawable2).getBitmap();

        Mat mat1 = new Mat();
        Mat mat2 = new Mat();

        Utils.bitmapToMat(bitmap1, mat1);
        Utils.bitmapToMat(bitmap2, mat2);

        Imgproc.cvtColor(mat1, mat1, Imgproc.COLOR_BGR2GRAY);
        Imgproc.cvtColor(mat2, mat2, Imgproc.COLOR_BGR2GRAY);

        int ret = compareFeature(mat1, mat2);

        if(ret > 0){
            Log.d("TAG", "Two images are same");
            Toast.makeText(this, "Two images are same", Toast.LENGTH_SHORT).show();
        }else{
            Log.d("TAG", "Two images are different");
            Toast.makeText(this, "Two images are different", Toast.LENGTH_SHORT).show();
        }
    }
}
