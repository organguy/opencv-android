package kr.co.jcsound.opencv_study;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.constraintlayout.solver.widgets.Rectangle;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class TrackbarColorActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private ImageView ivPallete;
    private Bitmap mInputImage;
    Mat mat;

    int colorB = 0;
    int colorG = 0;
    int colorR = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_trackbar_color);

        AppCompatSeekBar sbB = findViewById(R.id.sb_b);
        AppCompatSeekBar sbG = findViewById(R.id.sb_g);
        AppCompatSeekBar sbR = findViewById(R.id.sb_r);
        ivPallete = findViewById(R.id.iv_pallete);

        sbB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                colorB = i;
                drawColor();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbG.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                colorG = i;
                drawColor();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbR.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                colorR = i;
                drawColor();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void initOpenCV() {
        ivPallete.post(new Runnable() {
            @Override
            public void run() {
                mat = new Mat(ivPallete.getHeight(), ivPallete.getWidth(), CvType.CV_8UC3);
                mInputImage = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
            }
        });
    }

    public void drawColor(){
        Log.d(TAG, "drawCircle");
        Scalar color = new Scalar(colorB, colorG, colorR);

        Log.d(TAG, ivPallete.getWidth() + ", " + ivPallete.getHeight());

        Point startPoint = new Point(0, 0);
        Point endPoint = new Point(ivPallete.getWidth(), ivPallete.getHeight());

        Imgproc.rectangle(mat, startPoint, endPoint, color, -1);
        Utils.matToBitmap(mat, mInputImage);
        ivPallete.setImageBitmap(mInputImage);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();

        mInputImage.recycle();
        if (mInputImage != null) {
            mInputImage = null;
        }
    }
}
