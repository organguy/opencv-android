package kr.co.jcsound.opencv_study;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class PixelColorActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private static final int REQ_CODE_SELECT_IMAGE = 100;
    private Bitmap mInputImage;
    private ImageView mImageView;
    private boolean mIsOpenCVReady = false;
    private Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pixel_color);
        mImageView = findViewById(R.id.origin_iv);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(PERMISSIONS)) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String path = getImagePathFromURI(data.getData());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 4;
                    mInputImage = BitmapFactory.decodeFile(path, options);
                    if (mInputImage != null) {
                        mImageView.setImageBitmap(mInputImage);

                        if(mat == null){
                            mat = new Mat();
                        }

                        //Utils.bitmapToMat(((BitmapDrawable)mImageView.getDrawable()).getBitmap(), mat);

                        Utils.bitmapToMat(mInputImage, mat);

                        mImageView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {

                                switch (motionEvent.getAction()){
                                    case MotionEvent.ACTION_DOWN:
                                        //getPixelColor(motionEvent);
                                        changeColorToBlack(motionEvent);
                                        break;
                                }

                                return false;
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();

        mInputImage.recycle();
        if (mInputImage != null) {
            mInputImage = null;
        }
    }

    public void onButtonClicked(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
    }

    // permission
    static final int PERMISSIONS_REQUEST_CODE = 1000;
    String[] PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE"};


    private boolean hasPermissions(String[] permissions) {
        int result;
        for (String perms : permissions) {
            result = ContextCompat.checkSelfPermission(this, perms);
            if (result == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public String getImagePathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String imgPath = cursor.getString(idx);
            cursor.close();
            return imgPath;
        }
    }

    // permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraPermissionAccepted = grantResults[0]
                            == PackageManager.PERMISSION_GRANTED;

                    if (!cameraPermissionAccepted)
                        showDialogForPermission("실행을 위해 권한 허가가 필요합니다.");
                }
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(PixelColorActivity.this);
        builder.setTitle("알림");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        builder.create().show();
    }

    public void getPixelColor(MotionEvent e) {

        String clickPosition = "X : " + e.getX() + ", Y : " + e.getY();
        String size = "width : " + mat.size().width + ", height : " + mat.size().height;

        Log.d(TAG, size);
        Log.d(TAG, clickPosition);

        double[] colors = mat.get((int)e.getY(), (int)e.getX());

        if(colors != null && colors.length > 0){
            String resultColor = colors[0] + "," + colors[1] + "," + colors[2] + "," + colors[3];

            Log.d(TAG, resultColor);

            //setPixelColor(e);
        }
    }

    public void changeColorToBlack(MotionEvent e) {

        double[] colors = mat.get((int)e.getY(), (int)e.getX());

        for(int i = 0; i < mat.rows(); i++){
            for(int j = 0; j < mat.cols(); j++){

                double[] compareColors = mat.get(i, j);

                if(Arrays.equals(colors, compareColors)){

                    Log.d(TAG, "changeColorToBlack");

                    double[] changeColors = mat.get(i, j);

                    changeColors[0] = 0;
                    changeColors[1] = 0;
                    changeColors[2] = 0;

                    mat.put(i, j, changeColors);
                }
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(mat, bitmap);

        mImageView.setImageBitmap(bitmap);
    }

    public void setPixelColor(MotionEvent e) {

        double[] changeColors = mat.get((int)e.getY(), (int)e.getX());

        changeColors[0] = 255;
        changeColors[1] = 255;
        changeColors[2] = 255;
        mat.put((int)e.getY(), (int)e.getX(), changeColors);

        double[] colors = mat.get((int)e.getY(), (int)e.getX());

        if(colors != null && colors.length > 0){
            String resultColor = colors[0] + "," + colors[1] + "," + colors[2] + "," + colors[3];
            Log.d(TAG, resultColor);
        }

        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(mat, bitmap);

        mImageView.setImageBitmap(bitmap);
    }
}
