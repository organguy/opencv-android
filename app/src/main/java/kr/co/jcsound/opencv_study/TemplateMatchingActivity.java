package kr.co.jcsound.opencv_study;

import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class TemplateMatchingActivity extends AppCompatActivity
        implements CameraBridgeViewBase.CvCameraViewListener2 {

    /**
     * the camera view
     */
    private CameraBridgeViewBase mOpenCvCameraView;

    /**
     * the template image to use
     */
    private static final int TEMPLATE_IMAGE = R.drawable.pxl;

    /**
     * the result matrix
     */
    Mat result;


    /**
     * the template image used for template matching
     * or for copying into the camera view
     */
    Mat templ;

    /**
     * the camera image
     */
    Mat img;


    /**
     * the crop rectangle with the size of the template image
     */
    Rect rect;

    /**
     * selected area is the camera preview cut to the crop rectangle
     */
    Mat selectedArea;

    /**
     * frame size width
     */
    private static final int FRAME_SIZE_WIDTH = 640;
    /**
     * frame size height
     */
    private static final int FRAME_SIZE_HEIGHT = 480;
    /**
     * whether or not to use a fixed frame size -> results usually in higher FPS
     * 640 x 480
     */
    private static final boolean FIXED_FRAME_SIZE = true;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case BaseLoaderCallback.SUCCESS:
                    Log.d("TAG", "OpenCV loaded successfully");

                    // load the specified image from file system in bgr color
                    Mat bgr = null;
                    try {
                        bgr = Utils.loadResource(getApplicationContext(), TEMPLATE_IMAGE, Imgcodecs.IMREAD_COLOR);
                    }catch (IOException e){
                        Toast.makeText(mAppContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }

                    // convert the image to rgba
                    templ = new Mat();
                    Imgproc.cvtColor(bgr, templ, Imgproc.COLOR_BGR2GRAY);

                    // init the crop rectangle, necessary for copying the image to the camera view
                    rect = new Rect(0, 0, templ.width(), templ.height());

                    // init the result matrix
                    result = new Mat();
                    img = new Mat();

                    mOpenCvCameraView.enableView();

                    break;

                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_template_matching);

        mOpenCvCameraView = findViewById(R.id.tutorial1_activity_java_surface_view);

        if(FIXED_FRAME_SIZE){
            mOpenCvCameraView.setMaxFrameSize(FRAME_SIZE_WIDTH, FRAME_SIZE_HEIGHT);;
        }

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!OpenCVLoader.initDebug()){
            Log.d("TAG", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        }else{
            Log.d("TAG", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mOpenCvCameraView != null){
            mOpenCvCameraView.disableView();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        img = inputFrame.gray();

        /// Source image to display
        Mat img_display = new Mat();
        img.copyTo(img_display);

        int result_cols = img.cols() - templ.cols() + 1;
        int result_rows = img.rows() - templ.rows() + 1;
        result.create(result_rows, result_cols, CvType.CV_32FC1);

        int match_method = Imgproc.TM_SQDIFF;
        Imgproc.matchTemplate(img, templ, result, match_method);

        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        Core.MinMaxLocResult minMaxLocResult = Core.minMaxLoc(result, new Mat());

        Point matchLoc = null;

        if(match_method == Imgproc.TM_SQDIFF || match_method == Imgproc.TM_SQDIFF_NORMED){
            matchLoc = minMaxLocResult.minLoc;
        }else{
            matchLoc = minMaxLocResult.maxLoc;
        }

        Imgproc.rectangle(img_display, matchLoc, new Point(matchLoc.x + templ.cols(), matchLoc.y + templ.rows()), new Scalar(255, 0, 0));
        Imgproc.rectangle(result, matchLoc, new Point(matchLoc.x + templ.cols(), matchLoc.y + templ.rows()), new Scalar(255, 0, 0));

        return img_display;
    }
}