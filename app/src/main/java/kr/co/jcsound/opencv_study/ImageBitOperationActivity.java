package kr.co.jcsound.opencv_study;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class ImageBitOperationActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private static final int REQ_CODE_SELECT_IMAGE = 100;
    private ImageView ivOperation;
    private boolean mIsOpenCVReady = false;
    private Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bit_operation_image);
        ivOperation = findViewById(R.id.iv_operation);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(PERMISSIONS)) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    // permission
    static final int PERMISSIONS_REQUEST_CODE = 1000;
    String[] PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE"};


    private boolean hasPermissions(String[] permissions) {
        int result;
        for (String perms : permissions) {
            result = ContextCompat.checkSelfPermission(this, perms);
            if (result == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    // permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraPermissionAccepted = grantResults[0]
                            == PackageManager.PERMISSION_GRANTED;

                    if (!cameraPermissionAccepted)
                        showDialogForPermission("실행을 위해 권한 허가가 필요합니다.");
                }
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ImageBitOperationActivity.this);
        builder.setTitle("알림");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        builder.create().show();
    }

    @Override
    public void initOpenCV() {
        bitOperationImage(10, 10);
    }

    public void bitOperationImage(int xPos, int yPos){

        Drawable suziDrawable = getResources().getDrawable(R.drawable.suzi, null);
        Drawable openCvDrawable = getResources().getDrawable(R.drawable.opencv, null);

        Bitmap bpSuzi = ((BitmapDrawable)suziDrawable).getBitmap();
        Bitmap bpOpenCv = ((BitmapDrawable)openCvDrawable).getBitmap();

        Mat matSuzi = new Mat();
        Mat matOpenCv = new Mat();


        //Mat matAdd = new Mat();

        Utils.bitmapToMat(bpOpenCv, matOpenCv);
        Utils.bitmapToMat(bpSuzi, matSuzi);

        Mat roi = matSuzi.submat(yPos, yPos + matOpenCv.rows(), xPos, xPos + matOpenCv.cols());

        Mat matOpenCvGray = new Mat();
        Imgproc.cvtColor(matOpenCv, matOpenCvGray, Imgproc.COLOR_BGR2GRAY);

        Mat matOpenCvMask = new Mat();
        Imgproc.threshold(matOpenCvGray, matOpenCvMask, 10, 255, Imgproc.THRESH_BINARY);

        Mat matOpencCvMaskInv = new Mat();
        Core.bitwise_not(matOpenCvMask, matOpencCvMaskInv);

        Mat matSuziBg = new Mat();
        Core.bitwise_and(roi, roi, matSuziBg, matOpencCvMaskInv);

        Mat matOpenCvFg = new Mat();
        Core.bitwise_and(matOpenCv, matOpenCv, matOpenCvFg, matOpenCvMask);

        Mat matOpenCvResult = new Mat();
        Core.add(matSuziBg, matOpenCvFg, matOpenCvResult);

        Mat matResult = new Mat();
        overlayImage(matSuzi, matOpenCvResult, matResult, new Point(10, 10));

        Bitmap bpResult = Bitmap.createBitmap(matResult.cols(), matResult.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matResult, bpResult);

        ivOperation.setImageBitmap(bpResult);
    }

    public static void overlayImage(Mat background, Mat foreground, Mat output, Point location){

        background.copyTo(output);

        for(int y = (int) Math.max(location.y , 0); y < background.rows(); ++y){

            int fY = (int) (y - location.y);

            if(fY >= foreground.rows())
                break;

            for(int x = (int) Math.max(location.x, 0); x < background.cols(); ++x){
                int fX = (int) (x - location.x);
                if(fX >= foreground.cols()){
                    break;
                }

                double opacity;
                double[] finalPixelValue = new double[4];

                opacity = foreground.get(fY , fX)[3];

                finalPixelValue[0] = background.get(y, x)[0];
                finalPixelValue[1] = background.get(y, x)[1];
                finalPixelValue[2] = background.get(y, x)[2];
                finalPixelValue[3] = background.get(y, x)[3];

                for(int c = 0;  c < output.channels(); ++c){
                    if(opacity > 0){
                        double foregroundPx =  foreground.get(fY, fX)[c];
                        double backgroundPx =  background.get(y, x)[c];

                        float fOpacity = (float) (opacity / 255);
                        finalPixelValue[c] = ((backgroundPx * ( 1.0 - fOpacity)) + (foregroundPx * fOpacity));
                        if(c==3){
                            finalPixelValue[c] = foreground.get(fY,fX)[3];
                        }
                    }
                }
                output.put(y, x,finalPixelValue);
            }
        }
    }
}