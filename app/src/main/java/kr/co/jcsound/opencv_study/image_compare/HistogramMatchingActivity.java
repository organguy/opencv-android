package kr.co.jcsound.opencv_study.image_compare;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import kr.co.jcsound.ocvcannyedgedetection.R;
import kr.co.jcsound.opencv_study.OrganActivity;

public class HistogramMatchingActivity extends OrganActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public int compareHistogram(Mat img1, Mat img2){
        int retval = 0;
        long startTime = System.currentTimeMillis();

        Mat hsvImg1 = new Mat();
        Mat hsvImg2 = new Mat();

        Imgproc.cvtColor(img1, hsvImg1, Imgproc.COLOR_BGR2HSV);
        Imgproc.cvtColor(img2, hsvImg2, Imgproc.COLOR_BGR2HSV);

        List<Mat> listImg1 = new ArrayList<>();
        List<Mat> listImg2 = new ArrayList<>();

        listImg1.add(hsvImg1);
        listImg2.add(hsvImg2);

        MatOfFloat ranges = new MatOfFloat(0, 255);
        MatOfInt histSize = new MatOfInt(50);
        MatOfInt channels = new MatOfInt(0);

        Mat histImg1 = new Mat();
        Mat histImg2 = new Mat();

        Imgproc.calcHist(listImg1, channels, new Mat(), histImg1, histSize, ranges);
        Imgproc.calcHist(listImg2, channels, new Mat(), histImg2, histSize, ranges);

        Core.normalize(histImg1, histImg1, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Core.normalize(histImg2, histImg1, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        double result0, result1, result2, result3;
        result0 = Imgproc.compareHist(histImg1, histImg2, 0);
        result1 = Imgproc.compareHist(histImg1, histImg2, 1);
        result2 = Imgproc.compareHist(histImg1, histImg2, 2);
        result3 = Imgproc.compareHist(histImg1, histImg2, 3);

        int count = 0;
        if(result0 > 0.9){
            count++;
        }

        if(result1 < 0.1){
            count++;
        }

        if(result2 > 1.5){
            count++;
        }

        if(result3 < 0.3){
            count++;
        }

        if(count >= 3){
            retval = 1;
        }

        long estimateTime = System.currentTimeMillis() - startTime;

        Log.d("TAG", "Method[0] : " + result0);
        Log.d("TAG", "Method[1] : " + result1);
        Log.d("TAG", "Method[2] : " + result2);
        Log.d("TAG", "Method[3] : " + result3);
        Log.d("TAG", "result count : " + count);
        Log.d("TAG", "estimateTime : " + estimateTime + "ms");

        return retval;
    }

    @Override
    public void initOpenCV() {
        Drawable drawable1 = getResources().getDrawable(R.drawable.kcsj_001, null);
        Drawable drawable2 = getResources().getDrawable(R.drawable.ps01001001, null);

        Bitmap bitmap1 = ((BitmapDrawable)drawable1).getBitmap();
        Bitmap bitmap2 = ((BitmapDrawable)drawable2).getBitmap();

        Mat mat1 = new Mat();
        Mat mat2 = new Mat();

        Utils.bitmapToMat(bitmap1, mat1);
        Utils.bitmapToMat(bitmap2, mat2);

        int ret = compareHistogram(mat1, mat2);

        if(ret > 0){
            Log.d("TAG", "Two images are same");
            Toast.makeText(this, "Two images are same", Toast.LENGTH_SHORT).show();
        }else{
            Log.d("TAG", "Two images are different");
            Toast.makeText(this, "Two images are different", Toast.LENGTH_SHORT).show();
        }
    }
}
