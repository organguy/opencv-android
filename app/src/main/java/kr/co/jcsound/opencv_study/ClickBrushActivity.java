package kr.co.jcsound.opencv_study;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.Random;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class ClickBrushActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private FrameLayout flContainer;
    private ImageView ivImage;
    private Bitmap mInputImage;
    Mat mat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_click_brush);

        flContainer = findViewById(R.id.fl_container);
        ivImage = findViewById(R.id.iv_image);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initOpenCV() {
        mat = new Mat(getDeviceHeight(), getDeviceWidth(), CvType.CV_8UC3);
        mInputImage = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        ivImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        drawCircle(motionEvent);
                        break;
                }

                return false;
            }
        });
    }

    public void drawCircle(MotionEvent motionEvent){
        Log.d(TAG, "drawCircle");
        Scalar color = new Scalar(new Random().nextInt(257), new Random().nextInt(257), new Random().nextInt(257));
        Imgproc.circle(mat, new Point(motionEvent.getX(), motionEvent.getY()), 50, color, -1);
        Utils.matToBitmap(mat, mInputImage);
        ivImage.setImageBitmap(mInputImage);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();

        mInputImage.recycle();
        if (mInputImage != null) {
            mInputImage = null;
        }
    }
}
