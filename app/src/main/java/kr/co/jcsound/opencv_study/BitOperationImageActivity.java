package kr.co.jcsound.opencv_study;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class BitOperationImageActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private static final int REQ_CODE_SELECT_IMAGE = 100;
    private ImageView ivImage;
    private boolean mIsOpenCVReady = false;
    private Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bit_operation);
        ivImage = findViewById(R.id.iv_image);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(PERMISSIONS)) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    // permission
    static final int PERMISSIONS_REQUEST_CODE = 1000;
    String[] PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE"};


    private boolean hasPermissions(String[] permissions) {
        int result;
        for (String perms : permissions) {
            result = ContextCompat.checkSelfPermission(this, perms);
            if (result == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    // permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraPermissionAccepted = grantResults[0]
                            == PackageManager.PERMISSION_GRANTED;

                    if (!cameraPermissionAccepted)
                        showDialogForPermission("실행을 위해 권한 허가가 필요합니다.");
                }
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BitOperationImageActivity.this);
        builder.setTitle("알림");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        builder.create().show();
    }

    @Override
    public void initOpenCV() {

    }

    public void bitOperationImage(int hpos, int vpos){

        Drawable suziDrawable = getResources().getDrawable(R.drawable.suzi, null);
        Drawable opencvDrawable = getResources().getDrawable(R.drawable.opencv, null);

        Bitmap bpSuzi = ((BitmapDrawable)suziDrawable).getBitmap();
        Bitmap bpOpencv = ((BitmapDrawable)opencvDrawable).getBitmap();

        Mat matImg1 = new Mat();
        Mat matImg2 = new Mat();

        Utils.bitmapToMat(bpSuzi, matImg1);
        Utils.bitmapToMat(bpOpencv, matImg2);


        Mat roi = matImg1.submat(0, vpos, 0, hpos);

        Mat matImg2gray = new Mat();
        Imgproc.cvtColor(matImg2, matImg2gray, Imgproc.COLOR_BGR2GRAY);

        Mat matMask = new Mat();
        Imgproc.threshold(matImg2gray, matMask, 10, 255, Imgproc.THRESH_BINARY);

        Mat matMaskInv = new Mat();
        Core.bitwise_not(matMask, matMaskInv);

        Mat matImg1bg = new Mat();
        Core.bitwise_and(roi, roi, matImg1bg, matMaskInv);

        Mat matImg2fg = new Mat();
        Core.bitwise_and(matImg2, matImg2, matImg2fg, matMask);

        Mat matDst = new Mat();
        Core.add(matImg1bg, matImg2fg, matDst);




        /*matSuzi = matSuzi.submat(0, matBg.rows(), 0, matBg.cols());

        //Core.add(matBg, matSuzi, matBlending);

        Core.addWeighted(matBg, (double)(100 - blend) / 100.0, matSuzi, (double)blend / 100.0, 0, matBlending);

        Bitmap bpBlending = Bitmap.createBitmap(matBlending.cols(), matBlending.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(matBlending, bpBlending);

        ivBlending.setImageBitmap(bpBlending);*/
    }
}