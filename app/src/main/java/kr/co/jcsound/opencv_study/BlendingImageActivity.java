package kr.co.jcsound.opencv_study;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.core.content.ContextCompat;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class BlendingImageActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private static final int REQ_CODE_SELECT_IMAGE = 100;
    private ImageView ivBlending;
    private boolean mIsOpenCVReady = false;
    private Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_blending_image);
        ivBlending = findViewById(R.id.iv_blending);

        AppCompatSeekBar sbBlending = findViewById(R.id.sb_b);

        sbBlending.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                blendingImage(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(PERMISSIONS)) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    // permission
    static final int PERMISSIONS_REQUEST_CODE = 1000;
    String[] PERMISSIONS = {"android.permission.READ_EXTERNAL_STORAGE"};


    private boolean hasPermissions(String[] permissions) {
        int result;
        for (String perms : permissions) {
            result = ContextCompat.checkSelfPermission(this, perms);
            if (result == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    // permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraPermissionAccepted = grantResults[0]
                            == PackageManager.PERMISSION_GRANTED;

                    if (!cameraPermissionAccepted)
                        showDialogForPermission("실행을 위해 권한 허가가 필요합니다.");
                }
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BlendingImageActivity.this);
        builder.setTitle("알림");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        builder.create().show();
    }

    @Override
    public void initOpenCV() {

    }

    public void blendingImage(int blend){

        Drawable bgDrawable = getResources().getDrawable(R.drawable.bg, null);
        Drawable suziDrawable = getResources().getDrawable(R.drawable.suzi, null);

        Bitmap bpBg = ((BitmapDrawable)bgDrawable).getBitmap();
        Bitmap bpSuzi = ((BitmapDrawable)suziDrawable).getBitmap();

        Mat matBg = new Mat();
        Mat matSuzi = new Mat();
        Mat matBlending = new Mat();

        Utils.bitmapToMat(bpBg, matBg);
        Utils.bitmapToMat(bpSuzi, matSuzi);

        matSuzi = matSuzi.submat(0, matBg.rows(), 0, matBg.cols());

        //Core.add(matBg, matSuzi, matBlending);

        Core.addWeighted(matBg, (double)(100 - blend) / 100.0, matSuzi, (double)blend / 100.0, 0, matBlending);

        Bitmap bpBlending = Bitmap.createBitmap(matBlending.cols(), matBlending.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(matBlending, bpBlending);

        ivBlending.setImageBitmap(bpBlending);
    }
}