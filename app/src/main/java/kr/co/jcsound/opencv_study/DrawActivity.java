package kr.co.jcsound.opencv_study;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import kr.co.jcsound.ocvcannyedgedetection.R;


public class DrawActivity extends OrganActivity {

    private static final String TAG = "AndroidOpenCv";
    private Bitmap mInputImage;
    private ImageView mImageView;
    Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_draw);
        mImageView = findViewById(R.id.origin_iv);
    }

    public void draw(){
        mat = new Mat(512, 512, CvType.CV_8UC3);


        mInputImage = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);

        Imgproc.line(mat, new Point(0, 0), new Point(511, 511), new Scalar(255, 0, 0), 5);
        Imgproc.rectangle(mat, new Point(384, 0), new Point(510, 128), new Scalar(0, 255, 0), 3);
        Imgproc.circle(mat, new Point(477, 63), 63, new Scalar(0, 0, 255), -1);
        Imgproc.ellipse(mat, new Point(256, 256), new Size(100, 50), 0, 0, 180, new Scalar(0, 0, 255), -1);
        Imgproc.putText(mat, "OpenCV", new Point(10, 500), Imgproc.FONT_HERSHEY_SIMPLEX, 4, new Scalar(255, 255, 255), 2);

        Utils.matToBitmap(mat, mInputImage);

        mat.release();

        mImageView.setImageBitmap(mInputImage);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (OpenCVLoader.initDebug()) {
            draw();
        }
    }

    public void onDestroy() {
        super.onDestroy();

        mInputImage.recycle();
        if (mInputImage != null) {
            mInputImage = null;
        }
    }
}
