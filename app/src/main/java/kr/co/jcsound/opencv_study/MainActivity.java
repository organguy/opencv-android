package kr.co.jcsound.opencv_study;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import kr.co.jcsound.ocvcannyedgedetection.R;
import kr.co.jcsound.opencv_study.image_compare.FeatureMatchingActivity;
import kr.co.jcsound.opencv_study.image_compare.HistogramMatchingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_main2);

    }


    public void onButtonClicked(View view) {

        Intent intent = null;

        switch (view.getId()){
            case R.id.bt_detect_edge:
                intent = new Intent(getBaseContext(), DetectEdgeActivity.class);
                break;

            case R.id.bt_image_gray:
                intent = new Intent(getBaseContext(), ImageReadActivity.class);
                break;

            case R.id.bt_rear_camera:
                intent = new Intent(getBaseContext(), CameraActivity.class);
                intent.putExtra("mode", 0);
                break;

            case R.id.bt_front_camera:
                intent = new Intent(getBaseContext(), CameraActivity.class);
                intent.putExtra("mode", 1);
                break;

            case R.id.bt_gray_camera:
                intent = new Intent(getBaseContext(), CameraActivity.class);
                intent.putExtra("mode", 2);
                break;

            case R.id.bt_video_capture:
                intent = new Intent(getBaseContext(), VideoCaptureActivity.class);
                break;

            case R.id.bt_draw:
                intent = new Intent(getBaseContext(), DrawActivity.class);
                break;

            case R.id.bt_brush:
                intent = new Intent(getBaseContext(), ClickBrushActivity.class);
                break;

            case R.id.bt_trackbar_color:
                intent = new Intent(getBaseContext(), TrackbarColorActivity.class);
                break;

            case R.id.bt_pixel_color:
                intent = new Intent(getBaseContext(), PixelColorActivity.class);
                break;

            case R.id.bt_roi:
                intent = new Intent(getBaseContext(), RoiActivity.class);
                break;

            case R.id.bt_split:
                intent = new Intent(getBaseContext(), SplitChannelActivity.class);
                break;

            case R.id.bt_add_image:
                intent = new Intent(getBaseContext(), AddImageActivity.class);
                break;

            case R.id.bt_blending_image:
                intent = new Intent(getBaseContext(), BlendingImageActivity.class);
                break;

            case R.id.bt_bit_operation:
                intent = new Intent(getBaseContext(), ImageBitOperationActivity.class);
                break;

            case R.id.bt_template_matching:
                intent = new Intent(getBaseContext(), TemplateMatchingActivity.class);
                break;

            case R.id.bt_feature_matching:
                intent = new Intent(getBaseContext(), FeatureMatchingActivity.class);
                break;

            case R.id.bt_histogram_matching:
                intent = new Intent(getBaseContext(), HistogramMatchingActivity.class);
                break;


        }

        if(intent != null)
            startActivity(intent);
    }
}
